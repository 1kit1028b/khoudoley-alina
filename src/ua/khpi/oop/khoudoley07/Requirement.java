package ua.khpi.oop.khoudoley07;

public class Requirement {
	Requirement(){}
	
	private String specialty;
	private int experience;
	private String education;
	
	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String a) {
		specialty = a;
	}
	
	public int getExperience() {
		return experience;
	}

	public void setExperience(int a) {
		experience = a;
	}
	
	public String getEducation() {
		return education;
	}

	public void setEducation(String a) {
		education = a;
	}
}
