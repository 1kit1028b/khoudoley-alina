package ua.khpi.oop.khoudoley11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class Helper {
	void sortByFirm (LinkedList<Vacancy>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getFirm();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by firm : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortBySpecialty (LinkedList<Vacancy>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getSpecialty();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by specialty : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortByEducation (LinkedList<Vacancy>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getEducation();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by education : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void readFromFile(LinkedList<Vacancy>agency) throws IOException, ClassNotFoundException{
		String str = null;
		String str1 = null;
		String string = null;
		String firm = null;
		String specialty = null;
		int working_conditions = 0;
		int wage = 0;
		int experience = 0;
		String education = null;
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-ߥ�����-������]+[ ]?[�-ߥ�����-������]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[1-9]{1}[0-9]{3,}";
		String regex4 = "[1-9]{1}[0-9]?";
		BufferedReader br = new BufferedReader(new FileReader("file.txt"));
		BufferedReader br1 = new BufferedReader(new FileReader("file.txt"));
		int character;
		StringBuilder sb = new StringBuilder();
			while ((str = br.readLine()) != null) {
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex1);
				if (result  == false) {
					System.out.println("�� ����� ����������� ����� �����. ������ �� ���.");
					break;
				}
				firm = sb.toString();
				sb = new StringBuilder();
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex1);
				if (result  == false) {
					System.out.println("�� ����� ����������� �������������. ������ �� ���.");
					break;
				}
				specialty = sb.toString();
				sb = new StringBuilder();
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				int n = Integer.parseInt(sb.toString());
				str1 = Integer.toString(n);
				result = str1.matches(regex2);
				if (result  == false) {
					System.out.println("�� ����� ����������� ������� ����� �� �������. ������ �� ���.");
					break;
				}
				working_conditions = n;
				sb = new StringBuilder();
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				n = Integer.parseInt(sb.toString());
				str1 = Integer.toString(n);
				result = str1.matches(regex3);
				if (result  == false) {
					System.out.println("�� ����� ����������� ��������. ������ �� ���.");
					break;
				}
				wage = n;
				sb = new StringBuilder();
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				n = Integer.parseInt(sb.toString());
				str1 = Integer.toString(n);
				result = str1.matches(regex4);
				if (result  == false) {
					System.out.println("�� ����� ������������ ���� (����). ������ �� ���.");
					break;
				}
				experience = n;
				sb = new StringBuilder();
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',' || character == '\n')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex1);
				if (result  == false) {
					System.out.println("�� ����� ����������� �����. ������ �� ���.");
					break;
				}
				education = sb.toString();
				sb = new StringBuilder();
				
				Vacancy v = new Vacancy(firm, specialty, working_conditions, wage, experience, education);
				agency.add(v);
			}
	}
}
