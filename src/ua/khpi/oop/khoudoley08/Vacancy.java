package ua.khpi.oop.khoudoley08;

import java.io.*;

public class Vacancy implements Externalizable {
	Vacancy(int size) {
		// Vacancy vacancy = new Vacancy[size];
	}

	public Vacancy() {
	}

	private String firm;
	private String specialty;
	private int working_conditions;
	private int wage;
	Requirement r = new Requirement();

	public String getFirm() {
		return firm;
	}

	public void setFirm(String a) {
		firm = a;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String a) {
		specialty = a;
	}

	public int getWorking_conditions() {
		return working_conditions;
	}

	public void setWorking_conditions(int a) {
		working_conditions = a;
	}

	public int getWage() {
		return wage;
	}

	public void setWage(int a) {
		wage = a;
	}

	public void print(Vacancy a) {
		System.out.println("Գ���: " + a.getFirm() + ", �������������: " + a.getSpecialty() + ", ����� �����: "
				+ a.getWorking_conditions() + ", ��������: " + a.getWage() + ", ������ �� �������: �������������: "
				+ a.r.getSpecialty() + ", ����: " + a.r.getExperience() + ", �����: " + a.r.getEducation());
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(firm);
		out.writeObject(specialty);
		out.writeObject(working_conditions);
		out.writeObject(wage);
	}
	
	@Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firm = (String) in.readObject();
        specialty = (String) in.readObject();
        working_conditions = (int) in.readObject();
        wage = (int) in.readObject();
    }

}
