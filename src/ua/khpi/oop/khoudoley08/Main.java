package ua.khpi.oop.khoudoley08;

import java.util.Scanner;
import java.util.*;
import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		ArrayList<Vacancy> vacancy = new ArrayList<>(20);
		Vacancy e = new Vacancy();
		Vacancy c = new Vacancy();
		e.setFirm("ѳ����");
		e.setSpecialty("���������");
		e.setWorking_conditions(40);
		e.setWage(11000);
		e.r.setSpecialty("���������");
		e.r.setExperience(1);
		e.r.setEducation("����");
		vacancy.add(e);

		c.setFirm("�����������");
		c.setSpecialty("��������");
		c.setWorking_conditions(40);
		c.setWage(16000);
		c.r.setSpecialty("��������");
		c.r.setExperience(2);
		c.r.setEducation("����");
		vacancy.add(c);

		System.out.println("Before: ");
		vacancy.get(0).print(vacancy.get(0));
		vacancy.get(1).print(vacancy.get(1));

		String path = new String();
		Scanner in = new Scanner(System.in);
		boolean loop = true;
		int number = 0;
		while (loop) {
			System.out.println("\n�� �� ������ �������?\n" + "1)�������� ��'����\n"
					+ "2)������� ��'���� �� ����������� ��\n" + "3)�����\n");
			number = in.nextInt();
			switch (number) {
			case 1:

				File[] drives = File.listRoots();
				if (drives != null && drives.length > 0) {
					for (File aDrive : drives) {
						System.out.println(aDrive);
					}
				}

				System.out.println("������ ������� ����: ");
				String str = null;
				str = in.nextLine();
				str = in.nextLine();
				files(str);

				boolean loop1 = true;
				while (loop1) {
					System.out.println("������ ������ �����: ");
					String str1 = null;
					str1 = in.nextLine();
					str += "\\" + str1;
					if (str1.equals("")) {
						break;
					} else {
						files(str);
					}
				}
				path = str + "//Beanarchive.xml";
				XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));

				encoder.writeObject(vacancy.get(0));
				encoder.writeObject(vacancy.get(1));
				encoder.close();
				break;
			case 2:
				XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));

				Vacancy m = (Vacancy) decoder.readObject();
				Vacancy n = (Vacancy) decoder.readObject();
				decoder.close();
				System.out.println("After: ");
				m.print(m);
				n.print(n);
				break;
			case 3:
				loop = false;
				in.close();
			}
		}

	}

	public static void files(String s) {
		File file = new File(s);
		String[] directories = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		System.out.println(Arrays.toString(directories));
	}

}
