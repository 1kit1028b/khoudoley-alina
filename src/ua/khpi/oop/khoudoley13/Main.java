package ua.khpi.oop.khoudoley13;

import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.lang.Runnable;

import ua.khpi.oop.khoudoley13.Vacancy;
import ua.khpi.oop.khoudoley13.Helper;
import ua.khpi.oop.khoudoley13.MyThread;


public class Main {
	static MyThread mSecondThread;
	static MyThread mThirdThread;
	static MyThread mFourthThread;
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		mSecondThread = new MyThread();	//�������� ������
		mSecondThread.start();					//������ ������
		mThirdThread = new MyThread();	//�������� ������
		mThirdThread.start();					//������ ������
		mFourthThread = new MyThread();	//�������� ������
		mFourthThread.start();					//������ ������
		
		System.out.println("������� ����� ��������...");
		
		Scanner in = new Scanner(System.in);
		LinkedList<Vacancy> agency = new LinkedList<Vacancy>();
		String str;
		String str1;
		int s = 0;
		boolean loop = true;

		String firm = null;
		String specialty = null;
		int working_conditions = 0;
		int wage = 0;
		int experience = 0;
		String education = null;
		String skills = null;
		Helper helper = new Helper();
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-ߥ�����-������1-9'�]+[ ]?[�-ߥ�����-������1-9'�]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[1-9]{1}[0-9]{3,}";
		String regex4 = "[1-9]{1}[0-9]?";
		String regex5 = "[�-ߥ����]{1}[�-ߥ�����-������1-9'�]+[ ]?[�-ߥ�����-������1-9'�]*[[,]*[ ]?[�-ߥ�����-������1-9'�]*]*";

		boolean contains1 = Arrays.asList(args).contains("auto");

		if (contains1 == true) {
			helper.readFromFile(agency);
			helper.sortByFirm(agency);
			helper.sortBySpecialty(agency);
			helper.sortByEducation(agency);
			mSecondThread.search(agency);
			System.out.println("������ ����������� ��� ���������(���):");
			int m = in.nextInt();
			mThirdThread.min(agency);
			try{
				Thread.sleep(m*1000); //�������� � ������� m ���.
			}catch(InterruptedException e){}
			
			mThirdThread.interrupt();
			mFourthThread.count(agency);
		}

		else {
			while (loop) {
				System.out.println("�� ������ ������ �������? ");
				str = in.nextLine();
				if (str.equals("���") || str.equals("���")) {
					System.out.println("������ �����: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� ����� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					firm = str;
					System.out.println("������ �������������: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� �������������. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					specialty = str;
					System.out.println("������ ����� ����� (������� ����� �� �������): ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex2);
					while(result == false) {
						System.out.println("�� ����� ����������� ������� ����� �� �������. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex2);
					}
					working_conditions = s;
					System.out.println("������ ��������: ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex3);
					while(result == false) {
						System.out.println("�� ����� ����������� ��������. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex3);
					}
					wage = s;
					System.out.println("������ ������ �� �������: ���� (����): ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex4);
					while(result == false) {
						System.out.println("�� ����� ������������ ����. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex4);
					}
					experience = s;
					System.out.println("������ �����: ");
					str = in.nextLine();
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					education = str;
					System.out.println("������ �������� �����: ");
					str = in.nextLine();
					result = str.matches(regex5);
					while(result == false) {
						System.out.println("�� ����� ����������� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex5);
					}
					skills = str;
					
					Vacancy vacancy = new Vacancy(firm, specialty, working_conditions, wage, experience, education, skills);
					agency.add(vacancy);
				} else
					loop = false;
			}
			helper.sortByFirm(agency);
			helper.sortBySpecialty(agency);
			helper.sortByEducation(agency);
			helper.search(agency);
		}


		for (int i = 0; i < agency.size(); i++) {
			System.out.println("�" + (i + 1));
			System.out.println(agency.get(i));
			System.out.println();
		}

		/*
		 * for (Vacancy vacancy : agency) System.out.println(vacancy);
		 */

		String path = "Beanarchive.xml";
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));
		for (int i = 0; i < agency.size(); i++) {
			encoder.writeObject(agency.get(i));
		}
		encoder.close();

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));
		LinkedList<Vacancy> agencyRestored = new LinkedList<Vacancy>();
		Vacancy a = new Vacancy();
		for (int i = 0; i < agency.size(); i++) {
			a = (Vacancy) decoder.readObject();
			agencyRestored.add(a);
		}
		decoder.close();
		System.out.println("After Restored: ");
		for (int i = 0; i < agencyRestored.size(); i++) {
			System.out.println(agencyRestored.get(i));
		}

	}
}
