package ua.khpi.oop.khoudoley06;

import java.io.*;
import java.util.Scanner;
import ua.khpi.oop.shevchenko03.*;


public class Class {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		
		
		/*Container container = new Container();
		//Container container1 = new Container();
		container.add("Hello, world");
		container.add("It's 7 am");
		container.add("My name is Alina");
		container.add("This is the third task");*/
		
		
		int number = 0;
		Container container = new Container();
		Scanner in = new Scanner(System.in);
		boolean loop = true;
		while (loop) {
			System.out.println("\nWhat would you like to do?\n" + "1)Add an element in container\n" + "2)See the data\n"
					+ "3)Remove an element\n" + "4)Clear the container\n" + "5)Compare elements\n" + "6)Sort elements\n"
					+ "7)Search element\n" + "8)Count the average length of strings\n" + "9)Delete characters in the string:\n"
					+ "10)Delete characters in the container:\n"+ "11)Exit\n");
			number = in.nextInt();
			switch (number) {
			case 1:
				System.out.println("Enter the string: ");
				String str = in.nextLine();				
				str = in.nextLine();
				container.add(str);
				break;
			case 2:
				for (String string : container) System.out.println(string);
				break;
			case 3: 
				System.out.println("Enter the string which you want to remove: ");
				String s = in.nextLine();
				s = in.nextLine();
				container.remove(s);
				System.out.println("Done.");
				break;
			case 4:
				container.clear();
				System.out.println("Done.");
				break;
			case 5:
				System.out.println("Enter the number of the first string which you want to compare: ");
				int m  = in.nextInt();
				System.out.println("Enter the number of the second string which you want to compare: ");
				int n  = in.nextInt();
				boolean b;
				b = container.compare(container, m, n);
				if (b == true)System.out.println("Strings are the same ");
				else System.out.println("Strings are not the same ");
				break;
			case 6:
				container.sorting(container);
				System.out.println("Done.");
				break;
			case 7:
				System.out.println("Enter the string which you want to find: ");
				String s1 = in.nextLine();
				s1 = in.nextLine();
				int v = container.search(container, s1);
				if(v == 0)System.out.println("The string isn't in the container");
				else System.out.println("The string was found at position : " + v);
				break;
			case 8:
				int averageLength = HelperClass1.averageLength(container);
				System.out.println("Average length of string = " + averageLength);
				break;
			case 9 :
				System.out.println("Enter the number of the string which you want to delete characters in: ");
				int u  = in.nextInt();
				container.currentSize = container.size();
				String str1 = HelperClass.DeleteCharacter(container.mas[u-1]);
				container.mas[u-1] = str1;
				break;
			case 10:
				int count1 = container.size();
				for(int i = 0; i < count1; i++) {
					String str2 = HelperClass.DeleteCharacter(container.mas[i]);
					container.mas[i] = str2;
				}
				break;
			case 11:
				loop = false;
				in.close();
			}
		}
	}
}
		
		//container.sorting(container);
		//for (String string : container) System.out.println(string);
		// container.add("This is the third task111");
		// container.contains("It's 7 am");
		// container.clear();
		// container.remove("This is the third task111");
		// container.containsAll(container, container1);
		// container.toArray();
		/*
		 * for (String string : container) System.out.println(string);
		 */

		/*
		 * ContainerIterator it = container.iterator(); while(it.hasNext()) {
		 * //it.next(); System.out.println(it.next()); }
		 */
		
		//������������ � ���� � ������� ������ ObjectOutputStream
		/*ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("container.out"));
		objectOutputStream.writeObject(container);
		objectOutputStream.close();

		// ������������� �� ����� � ������� ������ ObjectInputStream
		ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("container.out"));
		Container containerRestored = (Container) objectInputStream.readObject();
		objectInputStream.close();

		System.out.println("Before Serialize: " + "\n" + container + "\n");
		System.out.println("After Restored: " + "\n" + containerRestored + "\n");*/
		//int averageLength = HelperClass.averageLength(container);
		//System.out.println("Average length of string = " + averageLength);
	
