package ua.khpi.oop.khoudoley06;

import java.util.Arrays;
import java.io.*;

public class Container implements Iterable<String>, Serializable {
	final int size = 4;
	String mas[] = new String[size];
	String current = mas[0];
	int currentSize = 0;

	@Override
	public ContainerIterator iterator() {
		// TODO Auto-generated method stub
		return new ContainerIterator(this);
	}

	@Override
	public String toString() {
		// int i = 0;
		String s = new String();
		// return String.format("Container{" + mas[0]);
		for (int i = 0; i < currentSize + 1; i++) {
			s += String.format(mas[i] + "; ");
			// return String.format("Container{" + mas[0] + ", " + mas[1] + ", " + mas[2] +
			// ", " + mas[3] + "} ");
		}
		return s;
	}

	void add(String string) {
		currentSize = size();
		if (currentSize < size) {
			mas[currentSize] = string;
		}

		else {
			mas = Arrays.copyOf(mas, size * 2);
			mas[currentSize] = string;
		}
	}

	void clear() {
		mas = new String[size];
	}

	boolean remove(String string) {
		currentSize = size();
		for (int i = 0; i < currentSize; i++) {
			if (mas[i].equals(string)) {
				mas[i] = null;
				//mas[i+1]
				//break;
			}
			mas[i] = mas[i+1];
		}
		return false;
	}

	Object[] toArray() {
		Object[] object = mas;
		return object;
	}

	int size() {
		currentSize = 0;
		for (int i = 0; i < mas.length; i++) {
			if (mas[i] != null)
				currentSize++;
		}
		return currentSize;
	}

	boolean contains(String string) {
		for (int i = 0; i < size; i++) {
			if (mas[i] == string)
				return true;
		}
		return false;
	}

	boolean containsAll(Container container, Container container1) {
		if (container == container1) {
			return true;
		} else
			return false;
	}

	boolean compare(Container container, int m, int n) {
		if (container.mas[m - 1] == container.mas[n - 1])
			return true;
		else
			return false;
	}

	int search(Container container, String str) {
		boolean b;
		b = contains(str);
		int n = 0;
		if (b == true) {
			for (int i = 0; i < size; i++) {
				if (container.mas[i] == str)
					n = i + 1;
			}
			// return n;
		} else
			n = 0;
		return n;
	}

	void sorting(Container container) {
		/*
		 * ������� ���� ������ ��� ��������� �������� �������, ��� ��� ���������� ����
		 * ������ ��� ������ � ����� ��������� ������������ �������
		 */
		size();
		for (int i = currentSize - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				/*
				 * ���������� �������� �������, ���� ��� ����� ������������ �������, �� ������
				 * �������
				 */
				if (container.mas[j].length() > container.mas[j + 1].length()) {
					String tmp = container.mas[j];
					container.mas[j] = container.mas[j + 1];
					container.mas[j + 1] = tmp;
				}
			}
		}
	}
}
