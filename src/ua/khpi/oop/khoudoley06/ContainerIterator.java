package ua.khpi.oop.khoudoley06;

import java.util.Iterator;

public class ContainerIterator implements Iterator<String> {
	Container container;
	int currentNumber = 0;

	public ContainerIterator(Container container) {
		// TODO Auto-generated constructor stub
		this.container = container;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return currentNumber < this.container.mas.length;
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		/*
		 * String string = current.getData(); current = current.getNext();
		 */
		return this.container.mas[currentNumber++];
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}
}
