package ua.khpi.oop.khoudoley04;

import java.util.Scanner;
import java.util.Arrays;

/**
 * @mainpage
 * ����������� ������ �4
 * ����: ������������ ��������� �������� ��� ��������� Java SE
 * 
 * ����: ��������� ���������� ������ ������ � ������������ � ���������� ��������� ����� Java.
 * ������: 
 * 1) �������������� �������� ������ �������� ����������� ������ �3, �������� �� ��������� ������ ����������� ������� ������ ����������� � ������ ���������� ����:
 * �������� �����;
 * �������� �����;
 * ��������� ���������;
 * ����������� ����������;
 * 
 * ���������� �������� � �.�.
 * 2) ����������� ������� ��������� ���������� ����� ��� ���������� ������ ������ ��������:
 * �������� �-h� �� �-help�: ������������ ���������� ��� ������ ��������, ����������� (������������ ��������), ��������� ���� ������ ������ (������ ���� �� ��������� ���������� �����);
 * �������� �-d� �� �-debug�: � ������ ������ �������� ������������� �������� ����, �� ���������� ������������ �� �������� ������������� ��������: ����������� �����������, ������� �������� ������, �������� ���������� ������ �� ��.
 * 
 * @author Alina Khoudoley
 * @version 1.0 04/10/19
 * @data 04.10.19
 *
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Alina Khoudoley
 * @version 1.0 04/10/19
 */

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean contains1 = Arrays.asList(args).contains("help");
		boolean contains2 = Arrays.asList(args).contains("debug");
		
		if(contains1 == true) {
			System.out.println("����������� ������ �4\r\n" + 
					"  ����: ������������ ��������� �������� ��� ��������� Java SE\r\n" + 
					"  \r\n" + 
					"  ����: ��������� ���������� ������ ������ � ������������ � ���������� ��������� ����� Java.\r\n" + 
					"  ������: \r\n" + 
					"  1) �������������� �������� ������ �������� ����������� ������ �3, �������� �� ��������� ������ ����������� ������� ������ ����������� � ������ ���������� ����:\r\n" + 
					"  �������� �����;\r\n" + 
					"  �������� �����;\r\n" + 
					"  ��������� ���������;\r\n" + 
					"  ����������� ����������;\r\n" + 
					"  ���������� �������� � �.�.\r\n" + 
					"  2) ����������� ������� ��������� ���������� ����� ��� ���������� ������ ������ ��������:\r\n" + 
					"  �������� �-h� �� �-help�: ������������ ���������� ��� ������ ��������, ����������� (������������ ��������), ��������� ���� ������ ������ (������ ���� �� ��������� ���������� �����);\r\n" + 
					"  �������� �-d� �� �-debug�: � ������ ������ �������� ������������� �������� ����, �� ���������� ������������ �� �������� ������������� ��������: ����������� �����������, ������� �������� ������, �������� ���������� ������ �� ��.\n"+
					"  �����: ������ ����\n");
		}
		
		if(contains2 == true) {
			System.out.println("��������� �������� � ������ debug");
		}
		
		int number = 0;
		Scanner in = new Scanner(System.in);
		String mas[] = null;
		String mas1[] = new String[50];
		String mas2[] = new String[50];
		boolean loop = true;
		while (loop) {
			System.out.println("\nWhat would you like to do?\n" + "1)Input data\n" + "2)See the data\n"
					+ "3)Run programm\n" + "4)See the result\n" + "5)Exit\n");
			number = in.nextInt();
			switch (number) {
			case 1:
				System.out.println("How many strings do you want to input?");
				int amount = in.nextInt();
				mas = new String[amount];
				System.out.print("Input the string: ");
				mas[0] = in.nextLine();
				mas[0] = in.nextLine();
				for (int i = 1; i < amount; i++) {
					System.out.print("Input the string: ");
					mas[i] = in.nextLine();
				}
				break;
			case 2:
				if (mas != null) {
					int i = check(mas);
					for (int j = 0; j < i; j++) {
						System.out.println("The " + (j + 1) + " string: " + mas[j]);
					}
				} else
					System.out.println("Error. Input the data first");
				break;
			case 3:
				int i = 0, j = 0;
				if (mas != null) {
					int l[] = new int[50];
					int m = check(mas);
					for (int n = 0; n < m; n++) {
						l[n] = mas[n].length();
					}
					int allLength = 0;
					for (int n = 0; n < m; n++) {
						allLength += l[n];
					}
					int averageLength = allLength / m;
					for (int n = 0; n < m; n++) {
						method(mas[n], averageLength, i, j, mas1, mas2);
						i = check(mas1);
						j = check(mas2);
					}
				} else
					System.out.println("Error. Input the data first");
				break;
			case 4:
				if (mas != null) {
					i = check(mas1);
					j = check(mas2);
					System.out.println("The first group: ");
					for (int n = 0; n < i; n++) {
						System.out.println(mas1[n] + ", length = " + mas1[n].length());
					}
					System.out.println();
					System.out.println("The second group: ");
					for (int n = 0; n < j; n++) {
						System.out.println(mas2[n] + ", length = " + mas2[n].length());
					}
				} else
					System.out.println("Error. Input the data first");
				break;
			case 5:
				loop = false;
				in.close();
			}
		}
	}

	public static void method(String s, int l, int i, int j, String s1[], String s2[]) {
		if (s.length() < l) {
			s1[i] = s;
		} else {
			s2[j] = s;
		}
	}

	public static int check(String s1[]) {
		int count1 = 0;
		for (int i = 0; i < s1.length; i++) {
			if (s1[i] != null)
				count1++;
		}
		return count1;
	}
	
}
