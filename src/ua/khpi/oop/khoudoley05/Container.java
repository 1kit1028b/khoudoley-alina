package ua.khpi.oop.khoudoley05;

import java.util.Arrays;

public class Container implements Iterable<String> {
	final int size = 4;
	String mas[] = new String[size];
	String current = mas[0];
	int currentSize = 0;

	@Override
	public ContainerIterator iterator() {
		// TODO Auto-generated method stub
		return new ContainerIterator(this);
	}

	@Override
	public String toString() {
		return String.format("The array");
	}

	void add(String string) {
		currentSize = size();
		if (currentSize < size) {
			mas[currentSize] = string;
		}

		else {
			mas = Arrays.copyOf(mas, size * 2);
			mas[currentSize] = string;
		}
	}

	void clear() {
		mas = new String[size];
	}

	boolean remove(String string) {
		currentSize = size();
		for (int i = 0; i < currentSize; i++) {
			if (mas[i] == string) {
				mas[i] = null;
				break;
			}
		}
		return false;
	}

	Object[] toArray() {
		Object[] object = mas;
		return object;
	}

	int size() {
		currentSize = 0;
		for (int i = 0; i < mas.length; i++) {
			if (mas[i] != null)
				currentSize++;
		}
		return currentSize;
	}

	boolean contains(String string) {
		for (int i = 0; i < size; i++) {
			if (mas[i] == string)
				return true;
		}
		return false;
	}

	boolean containsAll(Container container,Container container1) {
		if (container == container1) {
			return true;
		} else
			return false;
	}
}
