package ua.khpi.oop.khoudoley05;

/**
 * @mainpage
 * ����������� ������ �5
 * �������� ������� ����������. ��������� 
 * ����: ������� ������� �������� ������� ���������� �� ������������ ���������. 
 * 1 ������
 * 1.	��������� ����-���������, �� ��������� ��� ���������� ���������� ����� �������� �.�. �3 � ������ ������ ����� � ��������� ���������, ��������� � ���� ��������. 
 * 2.	� ��������� ���������� �� ���������������� �������� ������: 
 * �	String toString() ������� ���� ���������� � ������ �����; 
 * �	void add(String string) ���� �������� ������� �� ���� ����������; 
 * �	void clear() ������� �� �������� � ����������; 
 * �	boolean remove(String string) ������� ������ ������� ��������� �������� � ����������; 
 * �	Object[] toArray() ������� �����, �� ������ �� �������� � ���������; 
 * �	int size() ������� ������� �������� � ���������; 
 * �	boolean contains(String string) ������� true, ���� ��������� ������ �������� �������; 
 * �	boolean containsAll(Container container) ������� true, ���� ��������� ������ �� �������� � ����������� � ����������; 
 * �	public Iterator<String> iterator() ������� �������� �������� �� Interface Iterable. 
 * 3.	� ���� ��������� �������� �� Interface Iterator ���������� ������: 
 * �	public boolean hasNext(); 
 * �	public String next(); 
 * �	public void remove(). 
 * 4.	���������������� ������ ��������� �� ��������� ����� while � for each. 
 * 5.	������������� ������������ ���������� (��������) � ��������� � Java Collections Framework.
 *
 * @author Alina Khoudoley
 * @version 1.0 16/10/19
 * @data 16/10/19
 * 
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Alina Khoudoley
 * @version 1.0 16/10/19
 */

public class Class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Container container = new Container();
		Container container1 = new Container();
		container.add("Hello, world");
		container.add("It's 7 am");
		container.add("My name is Alina");
		container.add("This is the third task");
		container.add("This is the third task111");
		container.contains("It's 7 am");
		// container.clear();
		container.remove("This is the third task111");
		container.containsAll(container, container1);
		// container.toArray();
		for (String string : container)
			System.out.println(string);
		
		ContainerIterator it = container.iterator();
		while(it.hasNext())
		{
			//it.next();
			System.out.println(it.next());
		}

	}

}
