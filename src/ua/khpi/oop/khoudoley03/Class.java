package ua.khpi.oop.khoudoley03;

/**
 * @mainpage
 * ����������� ������ �3
 * ����: ��������� �����. ������� ������ � �����.
 * 
 * ����: �������� ������� ���������� �����; ������� ������� �������� ���������� ����� � ������������� ������ � �����.
 * ������: 
 * 1) ��������� �� ���������������� ��������� �������� ����� Java � ���������� Eclipse ��� �������� ��������� ������ �� �������, �� ������� ���������� �� ������� ������� �� ������ �� 15 ���������� �� ������� ������ �������� � ������ �����.
 * 2) ��� �������� ���������� ����� ��������������� �������.
 * 3) ���������������� ������������ �ᒺ��� ����� StringBuilder ��� StringBuffer.
 * 4) ����������� ������������� (����������) ������������ - ��������� ������ ��������� ����� (��������� ������� ���������� �����, ���. Helper Class) �� ��� ������� ����� ��������������� �������� �������� ������.
 * 5) ������������� ��������������� ������ ������� ���������� ������: ����� ������ java.util.regex (Pattern, Matcher �� ��.), � ����� �������� ������ ����� String (matches, replace, replaceFirst, replaceAll, split).
 * 
 * @author Alina Khoudoley
 * @version 1.0 01/10/19
 * @data 01.10.19
 *
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Alina Khoudoley
 * @version 1.0 01/10/19
 */

public class Class {
	/**
	 * ����� main
	 * <p>
	 * ������������ ��������� �� 1. ������������ ������ 2. ����������� ��������
	 * ������� ����� 3. ���������� �� ��� ����� �������� ����� ����� 4. ���������
	 * ����� �� ����� 5. �������� ������� ����� � ������ ���� 6. ��������� �
	 * ������� ����� �� �� ������� �� ������
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a, b, c, d;
		a = "Hello, world";
		b = "It's 7 am";
		c = "My name is Alina";
		d = "This is the third task";
		int l1 = a.length();
		int l2 = b.length();
		int l3 = c.length();
		int l4 = d.length();
		int averageLength = (l1 + l2 + l3 + l4) / 4;
		int i = 0, j = 0;
		String mas1[] = new String[4];
		String mas2[] = new String[4];
		HelperClass2.method(a, averageLength, i, j, mas1, mas2);
		i = HelperClass2.check(mas1);
		j = HelperClass2.check(mas2);
		HelperClass2.method(b, averageLength, i, j, mas1, mas2);
		i = HelperClass2.check(mas1);
		j = HelperClass2.check(mas2);
		HelperClass2.method(c, averageLength, i, j, mas1, mas2);
		i = HelperClass2.check(mas1);
		j = HelperClass2.check(mas2);
		HelperClass2.method(d, averageLength, i, j, mas1, mas2);
		i = HelperClass2.check(mas1);
		j = HelperClass2.check(mas2);
		HelperClass2.print(mas1, mas2, i, j);
	}
}
