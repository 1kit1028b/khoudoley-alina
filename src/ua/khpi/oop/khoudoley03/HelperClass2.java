package ua.khpi.oop.khoudoley03;

public class HelperClass2 {
	public static void method(String s, int l, int i, int j, String s1[], String s2[]) {
		if (s.length() < l) {
			s1[i] = s;
		} else {
			s2[j] = s;
		}
	}

	public static int check(String s1[]) {
		int count1 = 0;
		for (int i = 0; i < s1.length; i++) {
			if (s1[i] != null)
				count1++;
		}
		return count1;
	}

	public static void print(String mas1[], String mas2[], int i, int j) {
		System.out.println("The first group: ");
		for (int n = 0; n < i; n++) {
			System.out.println(mas1[n] + ", length = " + mas1[n].length());
		}
		System.out.println();
		System.out.println("The second group: ");
		for (int n = 0; n < j; n++) {
			System.out.println(mas2[n] + ", length = " + mas2[n].length());
		}
	}
}
