package ua.khpi.oop.khoudoley02;

import java.util.Random;

/**
 * @mainpage
 * ����������� ������ �2
 * ����: ����������� ������������. ����� ��������� ������� �����.
 * 
 * ����: �������� ������� ���������� ������� ��� ��������� Java SE.
 * ������: 
 * 1) ��������� �� ���������������� �������� ����� Java � ���������� Eclipse ��� �������� ��������� ������ �� �������, �� ������� ���������� �� ������� ������� �� ������ �� 10 ���������� �� ������� ������ �������� � ������ �����.
 * 2) ��� ���������� ������� ����� ��������������� ��������� ���������������� ����� (java.util.Random) �� ����������� ������� (��������� ������ ��������) ����������� ���������� ������ ��������� ������.
 * 3) ����������� ��������� �� ������ ��������� ������� ������� ����� �� ���������� ��������� � ������ �������.
 * 4) ����������� ������������� (����������) ������������ � ����������� ������ ��������� ������ �� ��������� ��������� ������.
 * 5) ������������� ������������ ����� ���� String �� ������ ��� ����������� ������ ��������� ������.
 * 
 * @author Alina Khoudoley
 * @version 1.0 29/09/19
 * @data 29.09.19
 *
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Alina Khoudoley
 * @version 1.0 29/09/19
 */

public class Task {
	/**
	 * ����� main
	 * <p>
	 * ������������ ��������� �� 1. ������������ ������ 2. �����������
	 * ���������� 8-�������� ������ ����� 3. ����������� ���� ������ � ���� ��������
	 * ���� � ������ �����
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * int number = 0; number = (int) (10000000 + Math.random()*89999999);
		 * System.out.println(number);
		 */

		int min = 10000000;
		int max = 99999999;
		int diff = max - min;
		System.out.println("�����		    ���� ������ ����	���� �������� ����");
		for (int i = 0; i < 10; i++) {
			Random random = new Random();
			int number = random.nextInt(diff + 1);
			number += min;
			// System.out.println(number);
			int summa = sumOfEven(number);
			// System.out.println("���� ������ ���� = " +summa);
			int summa1 = sumOfOdd(number);
			// System.out.println("���� �������� ���� = " +summa1);
			System.out.println(number + "		" + summa + "			" + summa1);
		}
	}

	/**
	 * 
	 * @param n
	 * @return sum
	 */
	public static int sumOfEven(int n) { // ���� ������ ����
		int sum = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 == 0)
				sum += (t % 10);
			t /= 10;
		}
		return sum;
	}

	public static int sumOfOdd(int n) { // ���� �������� ����
		int sum = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 != 0)
				sum += (t % 10);
			t /= 10;
		}
		return sum;
	}

}
