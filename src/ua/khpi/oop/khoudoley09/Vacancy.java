package ua.khpi.oop.khoudoley09;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;

public class Vacancy implements Externalizable{
	public Vacancy() {}
	
	private String firm;
	private String specialty;
	private int working_conditions;
	private int wage;
	private int experience;
	private String education;
	
	
	public Vacancy(String firm, String specialty, int working_conditions, int wage, int experience, String education) {
		this.firm = firm;
		this.specialty = specialty;
		this.working_conditions = working_conditions;
		this.wage = wage;
		this.experience = experience;
		this.education = education;
	}

	@Override
	public String toString() {
		return "Vacancy [firm=" + firm + ", specialty=" + specialty + ", working_conditions=" + working_conditions
				+ ", wage=" + wage + ", experience=" + experience + ", education=" + education + "]";
	}

	public String getFirm() {
		return firm;
	}
	public void setFirm(String firm) {
		this.firm = firm;
	}
	public int getWorking_conditions() {
		return working_conditions;
	}
	public void setWorking_conditions(int working_conditions) {
		this.working_conditions = working_conditions;
	}
	public int getWage() {
		return wage;
	}
	public void setWage(int wage) {
		this.wage = wage;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(firm);
		out.writeObject(specialty);
		out.writeObject(working_conditions);
		out.writeObject(wage);
		out.writeObject(experience);
		out.writeObject(education);
	}
	
	@Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        firm = (String) in.readObject();
        specialty = (String) in.readObject();
        working_conditions = (int) in.readObject();
        wage = (int) in.readObject();
        experience = (int) in.readObject();
        education = (String) in.readObject();
    }
	
}
