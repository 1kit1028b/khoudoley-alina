package ua.khpi.oop.khoudoley16;

import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.*;

import ua.khpi.oop.khoudoley16.Vacancy;
import ua.khpi.oop.khoudoley16.Helper;
 

public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		LinkedList<Vacancy> agency = new LinkedList<Vacancy>();
		String str;
		String str1; 
		int s = 0;
		boolean loop = true;

		String firm = null;
		String specialty = null;
		int working_conditions = 0;
		int wage = 0;
		int experience = 0;
		String education = null;
		String skills = null;
		Helper helper = new Helper();
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-ߥ�����-������'�]+[ ]?[�-ߥ�����-������1-9'�]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[1-9]{1}[0-9]{3,}";
		String regex4 = "[1-9]{1}[0-9]?";
		String regex5 = "[�-ߥ����]{1}[�-ߥ�����-������'�]+[ ]?[�-ߥ�����-������1-9'�]*[[,]*[ ]?[�-ߥ�����-������1-9'�]*]*";

		args = new String[2];
		//System.out.println(Arrays.asList(args));
		args[0] = "auto";
		//Arrays.asList(args).add("auto");
		boolean contains1 = Arrays.asList(args).contains("auto");

		if (contains1 == true) {
			helper.readFromFile(agency);
			helper.sortByFirm(agency);
			helper.sortBySpecialty(agency);
			helper.sortByEducation(agency);
			helper.search(agency);
		}

		else {
			while (loop) {
				System.out.println("�� ������ ������ �������? ");
				str = in.nextLine();
				if (str.equals("���") || str.equals("���")) {
					System.out.println("������ �����: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� ����� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					firm = str;
					System.out.println("������ �������������: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� �������������. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					specialty = str;
					System.out.println("������ ����� ����� (������� ����� �� �������): ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex2);
					while(result == false) {
						System.out.println("�� ����� ����������� ������� ����� �� �������. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex2);
					}
					working_conditions = s;
					System.out.println("������ ��������: ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex3);
					while(result == false) {
						System.out.println("�� ����� ����������� ��������. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex3);
					}
					wage = s;
					System.out.println("������ ������ �� �������: ���� (����): ");
					s = in.nextInt();
					str1 = Integer.toString(s);
					result = str1.matches(regex4);
					while(result == false) {
						System.out.println("�� ����� ������������ ����. ������ �� ���.");
						s = in.nextInt();
						str1 = Integer.toString(s);
						result = str1.matches(regex4);
					}
					experience = s;
					System.out.println("������ �����: ");
					str = in.nextLine();
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ����������� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					education = str;
					System.out.println("������ �������� �����: ");
					str = in.nextLine();
					result = str.matches(regex5);
					while(result == false) {
						System.out.println("�� ����� ����������� �����. ������ �� ���.");
						str = in.nextLine();
						result = str.matches(regex5);
					}
					skills = str;
					
					Vacancy vacancy = new Vacancy(firm, specialty, working_conditions, wage, experience, education, skills);
					agency.add(vacancy);
				} else
					loop = false;
			}
			helper.sortByFirm(agency);
			helper.sortBySpecialty(agency);
			helper.sortByEducation(agency);
			helper.search(agency);
		}

		// agency.remove(0);
		// agency.removeAll(agency);
		// System.out.println("Linked list : " + agency);
		// agency.contains(w);

		for (int i = 0; i < agency.size(); i++) {
			System.out.println("�" + (i + 1));
			System.out.println(agency.get(i));
			System.out.println();
		}

		/*
		 * for (Vacancy vacancy : agency) System.out.println(vacancy);
		 */

		String path = "Beanarchive.xml";
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));
		for (int i = 0; i < agency.size(); i++) {
			encoder.writeObject(agency.get(i));
		}
		encoder.close();

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));
		LinkedList<Vacancy> agencyRestored = new LinkedList<Vacancy>();
		Vacancy a = new Vacancy();
		for (int i = 0; i < agency.size(); i++) {
			a = (Vacancy) decoder.readObject();
			agencyRestored.add(a);
		}
		decoder.close();
		System.out.println("After Restored: ");
		for (int i = 0; i < agencyRestored.size(); i++) {
			System.out.println(agencyRestored.get(i));
		}

		/*
		 * //������������ � ���� � ������� ������ ObjectOutputStream ObjectOutputStream
		 * objectOutputStream = new ObjectOutputStream(new
		 * FileOutputStream("container.out")); objectOutputStream.writeObject(agency);
		 * objectOutputStream.close();
		 * 
		 * // ������������� �� ����� � ������� ������ ObjectInputStream
		 * ObjectInputStream objectInputStream = new ObjectInputStream(new
		 * FileInputStream("container.out")); LinkedList<Vacancy> agencyRestored =
		 * (LinkedList<Vacancy>) objectInputStream.readObject();
		 * objectInputStream.close();
		 * 
		 * System.out.println("Before Serialize: " + "\n" + agency + "\n");
		 * System.out.println("After Restored: " + "\n" + agencyRestored + "\n");
		 */

	}

}

