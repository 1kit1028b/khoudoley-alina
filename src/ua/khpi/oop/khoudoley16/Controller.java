package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Label label1;

    @FXML
    private Label label2;

    @FXML
    private TextField input;

    @FXML
    private String[] args;

    @FXML
    private void handleButtonAction1(ActionEvent event) throws IOException, ClassNotFoundException {
        //String text = input.getText();
        label1.setText("Готово.");
        //Arrays.asList(args).add("auto");
        //Main.main(args);
        /*args = new String[2];
        args[0] = "auto";*/
        //Main.main(args);
    }

    @FXML
    private void handleButtonAction2(ActionEvent event) throws IOException, ClassNotFoundException {
        //String text = input.getText();
        label2.setText("Введіть дані");
        //label.setText("Hello" + text);
        //Arrays.asList(args).add("auto");
        //Main.main(args);
        /*args = new String[2];
        args[0] = "auto";*/
        //Main.main(args);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
