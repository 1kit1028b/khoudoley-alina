package ua.khpi.oop.khoudoley01;

/**
 * @mainpage
 * ����������� ������ �1
 * ����: ��������� �������� ����� Java. ���� �����, �������, �������� � ���������.
 * 
 * ����: ������������ � JDK ��������� Java SE �� ����������� �������� Eclipse IDE. 
 * ������: 
 * 1) ������� ��� ��������� ������ �� ��� Java � ���������� Eclipse. 
 * 2) ���������������� ��������� ��������� �������� �� ���������� ������ � ����� ������������, �� �������������� ��������� �� ������. 
 * 3) �������� ��������� � ������ �������� � ���������� ����� �� ��������� ��������� ����� JDK.
 * 
 * @author Alina Khoudoley
 * @version 1.0 25/09/19
 * @data 25.09.19
 *
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Alina Khoudoley
 * @version 1.0 25/09/19
 */

public class Class {
	/**
	 * ����� main
	 * <p>
	 * ������������ ��������� �� 1. ������������ ������ 2. ϳ�������� �������
	 * ������ � �������� ���� � ������ ������ 3. ϳ�������� ������� ������� �
	 * �������� ����� �����
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int book = 0x93;
		long number = 380975486123l;
		int two = 0b10111;
		int four = 013753;
		int numberStudent = 18;
		int newNumberStudent = numberStudent - 1;
		int ost = newNumberStudent % 26;
		int newOst = ost + 1;
		char symbol = 'a';
		for (int i = 0; i < newOst; i++) {
			symbol = (char) ('a' + i);
		}
		symbol = Character.toUpperCase(symbol);

		// System.out.println(newOst);
		// System.out.println(symbol);

		// int numberOfEven1 = 0; //������
		// int numberOfOdd1 = 0; //��������

		int a = numberOfEven(book);
		// System.out.println("���������� ������ ���� � ���������� book = " +a);
		a = numberOfOdd(book);
		// System.out.println("���������� �������� ���� � ���������� book = " +a);

		a = numberOfEven(number);
		// System.out.println("���������� ������ ���� � ���������� number = " +a);
		a = numberOfOdd(number);
		// System.out.println("���������� �������� ���� � ���������� number = " +a);

		a = numberOfEven(two);
		// System.out.println("���������� ������ ���� � ���������� two = " +a);
		a = numberOfOdd(two);
		// System.out.println("���������� �������� ���� � ���������� two = " +a);

		a = numberOfEven(four);
		// System.out.println("���������� ������ ���� � ���������� four = " +a);
		a = numberOfOdd(four);
		// System.out.println("���������� �������� ���� � ���������� four = " +a);

		a = numberOfEven(newOst);
		// System.out.println("���������� ������ ���� � ���������� newOst = " +a);
		a = numberOfOdd(newOst);
		// System.out.println("���������� �������� ���� � ���������� newOst = " +a);

		String b = binary(book);
		// System.out.println("���������� book � �������� �������: " +b);
		int c = numberOfOne(b);
		// System.out.println("���������� ������ � ���������� book � �������� �������: "
		// +c);

		b = binary(number);
		// System.out.println("���������� number � �������� �������: " +b);
		c = numberOfOne(b);
		// System.out.println("���������� ������ � ���������� number � �������� �������:
		// " +c);

		b = binary(two);
		// System.out.println("���������� two � �������� �������: " +b);
		c = numberOfOne(b);
		// System.out.println("���������� ������ � ���������� two � �������� �������: "
		// +c);

		b = binary(four);
		// System.out.println("���������� four � �������� �������: " +b);
		c = numberOfOne(b);
		// System.out.println("���������� ������ � ���������� four � �������� �������: "
		// +c);

		b = binary(newOst);
		// System.out.println("���������� newOst � �������� �������: " +b);
		c = numberOfOne(b);
		// System.out.println("���������� ������ � ���������� newOst � �������� �������:
		// " +c);
	}

	public static int numberOfEven(long n) {
		int count = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 == 0)
				count++;
			t /= 10;
		}
		return count;
	}

	public static int numberOfOdd(long n) {
		int count = 0;
		long t = n;
		while (t > 0) {
			if ((t % 10) % 2 != 0)
				count++;
			t /= 10;
		}
		return count;
	}

	/*
	 * public static long binary(long a) { long i = 0, b = 0; while(a !=0 ) { b =
	 * a%2; System.out.print(b); a = a/2; } System.out.println(); return b; }
	 */

	public static String binary(long a) {
		long b;
		String temp = "";
		while (a != 0) {
			b = a % 2;
			temp = b + temp;
			a = a / 2;
		}
		return temp;
	}

	public static int numberOfOne(String a) {
		int count = 0;
		for (int i = 0; i < a.length(); i++) {
			if (a.charAt(i) == '1')
				count++;
		}
		return count;
	}

}
